/**********************************************************************************
ContourTracker.cpp:
This file defines all the functions declared in the target tracker.

Copyright (C) 2012, "masoug", all rights reserved.

License Agreement For Open Source Computer Vision Library

 Copyright (C) 2000-2008, Intel Corporation, all rights reserved.

 Copyright (C) 2009-2011, Willow Garage Inc., all rights reserved.

 Third party copyrights are property of their respective owners.

 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:

   * Redistribution's of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.

   * Redistribution's in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.

   * The name of the copyright holders may not be used to endorse or promote
     products derived from this software without specific prior written permission.

 This software is provided by the copyright holders and contributors "as is" and
 any express or implied warranties, including, but not limited to, the implied
 warranties of merchantability and fitness for a particular purpose are disclaimed.
 In no event shall the Intel Corporation, "masoug" or contributors be liable for 
 any direct, indirect, incidental, special, exemplary, or consequential damages
 (including, but not limited to, procurement of substitute goods or services;
 loss of use, data, or profits; or business interruption) however caused
 and on any theory of liability, whether in contract, strict liability,
 or tort (including negligence or otherwise) arising in any way out of
 the use of this software, even if advised of the possibility of such damage.
 *********************************************************************************/

#include "ContourTracker.h"
#include "assert.h"

void PrintLogo()
{
	cout << " _     _   _______   _   _   _   _   __"   << endl;
	cout << "| |   | | |  ___  | | | | | | | | | / /"   << endl;
	cout << "| |___| | | |___| | | | | | | | | |/ /"    << endl;
	cout << "|  ___  | |  ___  | | | | | | | |   |"     << endl;
	cout << "| |   | | | |   | | | |_| |_| | | |\\ \\"  << endl;
	cout << "|_|   |_| |_|   |_| |_________| |_| \\_\\" << endl;
	cout << endl;
}

vector<Rect> FindPoly(Mat &img, unsigned int sides, unsigned int maxSides,
		unsigned int minArea, unsigned int maxArea)
{
	vector<vector<Point> > contours;
	vector<Point> poly;
	vector<Rect> result;

	findContours(img, contours, CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE,
			Point(0, 0));
	for (unsigned int i = 0; i < contours.size(); i++)
	{
		approxPolyDP(contours[i], poly, arcLength(contours[i], true) * 0.02, 0);
		if (poly.size() >= sides && poly.size() <= maxSides
				&& contourArea(poly) >= minArea && contourArea(poly) <= maxArea)
			result.push_back(boundingRect(poly));
	}
	return result;
}

void TrimToAspectRatio(vector<Rect> &rect, float ratio, float errorVal)
{
	/* Aspect ratio = x:y */
	float currentRatio = 0.0;
	vector<Rect> result;
	for (unsigned int i = 0; i < rect.size(); i++)
	{
		currentRatio = (rect[i].width) / (rect[i].height);
		if (fabs(ratio - currentRatio) < errorVal)
			result.push_back(rect[i]);
	}
	rect = result;
}

void NestedTarget(vector<Rect> &rect, Mat &img, unsigned int sides,
		unsigned int maxSides, unsigned int minArea, unsigned int maxArea)
{
	vector<Rect> result;
	Mat roi;
	for (unsigned int i = 0; i < rect.size(); i++)
	{
		roi = img(rect[i]);
		if (FindPoly(roi, sides, maxSides, minArea, maxArea).size() > 1)
			result.push_back(rect[i]);
	}
	rect = result;
}

Rect FindHighest(vector<Rect> rect)
{
	/* In case the vector is empty. */
	if (rect.size() < 1)
		return Rect();

	int minVal = rect[0].y;
	int index = 0;
	/* Ironically, the highest target has the lowest y-value. */
	for (unsigned int i = 0; i < rect.size(); i++)
	{
		if (rect[i].y < minVal)
		{
			minVal = rect[i].y;
			index = i;
		}
	}
	return rect[index];
}

void CompactRectangle(Mat &img, Rect rect, int length, Scalar col, int thick)
{
	/* Top left. */
	line(img, Point(rect.x, rect.y), Point(rect.x + length, rect.y), col,
			thick);
	line(img, Point(rect.x, rect.y), Point(rect.x, rect.y + length), col,
			thick);

	/* Top right. */
	line(img, Point(rect.x + rect.width, rect.y),
			Point(rect.x + rect.width - length, rect.y), col, thick);
	line(img, Point(rect.x + rect.width, rect.y),
			Point(rect.x + rect.width, rect.y + length), col, thick);

	/* Bottom left. */
	line(img, Point(rect.x, rect.y + rect.height),
			Point(rect.x, rect.y + rect.height - length), col, thick);
	line(img, Point(rect.x, rect.y + rect.height),
			Point(rect.x + length, rect.y + rect.height), col, thick);

	/* Bottom right. */
	line(img, Point(rect.x + rect.width, rect.y + rect.height),
			Point(rect.x + rect.width - length, rect.y + rect.height), col, thick);
	line(img, Point(rect.x + rect.width, rect.y + rect.height),
			Point(rect.x + rect.width, rect.y + rect.height - length), col, thick);
}

void RenderTargets(Mat &img, vector<Rect> targets, stringstream &info)
{
	/* Draw on the image. */
	for (unsigned int i = 0; i < targets.size(); i++)
	{
		/* Draw recognized targets. */
		CompactRectangle(img, targets[i], 7, CV_RGB(0, 255, 0), 1);
		info << targets[i].x << ", " << targets[i].y;
		putText(img, info.str(), Point(targets[i].x, targets[i].y - 3),
				FONT_HERSHEY_PLAIN, 1.0, CV_RGB(0, 255, 0), 1);
		info.str("");
		/* Draw distance from center. */
		info << abs(img.cols / 2 - (targets[i].x + targets[i].width / 2));
		putText(img, info.str(),
				Point(targets[i].x + (targets[i].width / 2) - 5,
						targets[i].y + targets[i].height / 2),
				FONT_HERSHEY_PLAIN, 1.0, CV_RGB(255, 0, 0), 1);
		info.str("");
	}
}

void RenderCenter(Mat &img)
{
	/* Draw center line. */
	line(img, Point(img.cols / 2, 0.0),
			Point(img.cols / 2, img.rows), CV_RGB(255, 0, 0));
}

void RotateVertically(Mat &img)
{
	/* Re-rotate the image. */
	warpAffine(img, img,
			getRotationMatrix2D(Point(img.cols / 2, img.cols / 2), 90.0,1.0),
			Size(img.rows, img.cols));
}

void TrimToClassifier(Mat &img, vector<Rect> &targets, CascadeClassifier &cascade)
{
	Mat frame;
	vector<Rect> result, temp;
	for (unsigned int i = 0; i < targets.size(); i++)
	{
		cvtColor(img(targets[i]), frame, CV_RGB2GRAY);
		equalizeHist(frame, frame);
		cascade.detectMultiScale(frame, temp, 1.1, CV_HAAR_SCALE_IMAGE);
		if (temp.size())
			result.push_back(targets[i]);
	}
	targets = result;
}

void TrimToHistogram()
{
    /* Using a given histogram, trim out targets without a matching histogram. */
}
