/**********************************************************************************
Hawk.cpp:
Main program file to startup and run the target detector.

Copyright (C) 2012, "masoug", all rights reserved.

License Agreement For Open Source Computer Vision Library

 Copyright (C) 2000-2008, Intel Corporation, all rights reserved.

 Copyright (C) 2009-2011, Willow Garage Inc., all rights reserved.

 Third party copyrights are property of their respective owners.

 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:

   * Redistribution's of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.

   * Redistribution's in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.

   * The name of the copyright holders may not be used to endorse or promote
     products derived from this software without specific prior written permission.

 This software is provided by the copyright holders and contributors "as is" and
 any express or implied warranties, including, but not limited to, the implied
 warranties of merchantability and fitness for a particular purpose are disclaimed.
 In no event shall the Intel Corporation, "masoug" or contributors be liable for 
 any direct, indirect, incidental, special, exemplary, or consequential damages
 (including, but not limited to, procurement of substitute goods or services;
 loss of use, data, or profits; or business interruption) however caused
 and on any theory of liability, whether in contract, strict liability,
 or tort (including negligence or otherwise) arising in any way out of
 the use of this software, even if advised of the possibility of such damage.
**********************************************************************************/

#include <iostream>
#include <sstream>
#include <vector>
#include <string>
#include <assert.h>
#include "stdlib.h"

#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv/cv.h"
#include "libfreenect_cv.h"
#include "libfreenect_sync.h"
#include "ContourTracker.h"
#include "Timer.h"

#define MIN_AREA 50
#define AS_ERROR 0.48
#define MAX_SIDES 15
#define MAX_AREA 640*480
#define BLUE_ALLIANCE 1
#define RED_ALLIANCE 2

using namespace std;
using namespace cv;

int main(int argc, char* argv[])
{
	/* Initialization. */
	PrintLogo();
	cout << "HAWK: Initializing..." << endl;
	/* Parameters. */
	bool silentMode = false;
	int alliance = 0;
	bool useClassifier = false;
	cout << "HAWK: Detected " << argc << " command-line params." << endl;
	for (int i = 0; i < argc; i++)
	{
		if (string(argv[i]) == "--silent")
			silentMode = true;
		else if (string(argv[i]) == "--red-alliance")
			alliance = RED_ALLIANCE;
		else if (string(argv[i]) == "--blue-alliance")
			alliance = BLUE_ALLIANCE;
		else if (string(argv[i]) == "--use-classifier")
			useClassifier = true;
	}
	cout << "HAWK: Startup parameters:" << endl;
	cout << "\tSilent Mode:\t" << silentMode << endl;
	cout << "\tAlliance:\t";
	if (alliance == RED_ALLIANCE)
		cout << "Red" << endl;
	else if (alliance == BLUE_ALLIANCE)
		cout << "Blue" << endl;
	else
		cout << "None." << endl;
	cout << "\tUse Classifier:\t" << useClassifier << endl;

	/* Basic configurations. */
	Mat frame, thresh;
	vector<Rect> targets;
	stringstream info;
	CascadeClassifier classifier;
	Rect mainTarget;
	bool useKinect = false;
	DECLARE_TIMING(timer);

	if (!silentMode)
	{
		namedWindow("Threshold", CV_WINDOW_NORMAL);
		namedWindow("Camera", CV_WINDOW_NORMAL);
		namedWindow("ROI", CV_WINDOW_NORMAL);
	}
	if (useClassifier && !classifier.load("Positive5.xml"))
	{
		cout << "HAWK:ERROR: Failed to find \"Positive5.xml\" classifier file." << endl;
		cout << "HAWK:ERROR: Thus, I will not use classifiers for detection." << endl;
		useClassifier = false;
	}
	/* Initialization. */
	VideoCapture camera(0);
	if (!camera.isOpened())
	{
		printf("HAWK: Failed to load default camera. Trying the Kinect...\n");
		if (freenect_sync_get_rgb_cv(0) != NULL)
		{
			printf("HAWK: Kinect is responsive. Using Kinect RGB camera.\n");
			useKinect = true;
		}
		else
		{
			printf("HAWK: Failed to load the Kinect. Exiting.\n");
			return -1;
		}
	}

	/* Main program loop. */
	while (waitKey(10) != 'q')
	{
		START_TIMING(timer);
		/* Grab video frame. */
		if (useKinect)
			frame = Mat(freenect_sync_get_rgb_cv(0));
		else
			camera >> frame;
		/* Rotate. */
		//RotateVertically(frame);
		/* Account for special cases. */
		cvtColor(frame, thresh, CV_RGB2GRAY);
		threshold(thresh, thresh, 232.5, 255.0, THRESH_BINARY);
		if (!silentMode)
			imshow("Threshold", thresh);

		/* Image processing. */
		targets = FindPoly(thresh, 4, MAX_SIDES, MIN_AREA, MAX_AREA);
		TrimToAspectRatio(targets, (24 / 16.5), AS_ERROR);
		NestedTarget(targets, thresh, 4, MAX_SIDES, MIN_AREA, MAX_AREA);
		if (useClassifier)
			TrimToClassifier(frame, targets, classifier);
		if (targets.size())
			mainTarget = FindHighest(targets);
		STOP_TIMING(timer);

		/* Render image. */
		if (!silentMode)
		{
			RenderTargets(frame, targets, info);
			/* Indicate the highest target. */
			if (targets.size())
				imshow("ROI", frame(mainTarget));
			RenderCenter(frame);
			/* Display debug info. */
			info << "Processing time: " << GET_TIMING(timer) << " ms.";
			putText(frame, info.str(), Point(5, 10), FONT_HERSHEY_PLAIN, 0.75,
					CV_RGB(0, 255, 0), 1);
			info.str("");
			/* Show image. */
			imshow("Camera", frame);
		}
	}
	printf("HAWK: Average processing time: %f ms.\n",
			GET_AVERAGE_TIMING(timer));
	return 0;
}
