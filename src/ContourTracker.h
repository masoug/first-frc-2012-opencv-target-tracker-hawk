/**********************************************************************************
ContourTracker.h:
This is the main header that declares all the functions defined in the target 
tracker.

Copyright (C) 2012, "masoug", all rights reserved.

License Agreement For Open Source Computer Vision Library

 Copyright (C) 2000-2008, Intel Corporation, all rights reserved.

 Copyright (C) 2009-2011, Willow Garage Inc., all rights reserved.

 Third party copyrights are property of their respective owners.

 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:

   * Redistribution's of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.

   * Redistribution's in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.

   * The name of the copyright holders may not be used to endorse or promote
     products derived from this software without specific prior written permission.

 This software is provided by the copyright holders and contributors "as is" and
 any express or implied warranties, including, but not limited to, the implied
 warranties of merchantability and fitness for a particular purpose are disclaimed.
 In no event shall the Intel Corporation, "masoug" or contributors be liable for 
 any direct, indirect, incidental, special, exemplary, or consequential damages
 (including, but not limited to, procurement of substitute goods or services;
 loss of use, data, or profits; or business interruption) however caused
 and on any theory of liability, whether in contract, strict liability,
 or tort (including negligence or otherwise) arising in any way out of
 the use of this software, even if advised of the possibility of such damage.
**********************************************************************************/

#ifndef CONTOURTRACKER_H_
#define CONTOURTRACKER_H_

#include <iostream>
#include <vector>

#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv/cv.h"

using namespace std;
using namespace cv;

/* Eye candy. */
void PrintLogo();

/* Find n-sided polygons and returns bounding rectangles. */
vector<Rect> FindPoly(Mat &img, unsigned int sides, unsigned int minArea, unsigned int maxSides, unsigned int maxArea);

/* Removes elements of the array which are not the specified aspect ratio. */
void TrimToAspectRatio(vector<Rect> &rect, float ratio, float errorVal);

/* Checks if given image with ROI has concentric rectangle. */
void NestedTarget(vector<Rect> &rect, Mat &img, unsigned int sides, unsigned int maxSides, unsigned int minArea, unsigned int maxArea);

/* Returns the highest target in the array. */
Rect FindHighest(vector<Rect> rect);

/* Renders bounding rectangles as compact corners. */
void CompactRectangle(Mat &img, Rect rect, int length, Scalar col, int thick);

/* Renders the found targets. */
void RenderTargets(Mat &img, vector<Rect> targets, stringstream &info);

/* Renders the horizontal center of the image as a line. */
void RenderCenter(Mat &img);

/* Rotated the image 90 degrees. */
void RotateVertically(Mat &img);

/* Use Haar Cascade classification techniques to determine if target exits. */
void TrimToClassifier(Mat &img, vector<Rect> &targets, CascadeClassifier &cascade);

/* Use a histogram to see how close a given image matches target. */
void TrimToHistogram();


#endif /* CONTOURTRACKER_H_ */
