# FIRST FRC 2012 Basketball Hoop Target Detector. (HAWK) #
## Version: BETA - Functional, but may be buggy and needs tuning. ##

**Description:** Powered by [OpenCV](http://opencv.willowgarage.com/wiki/) (*2.3.1 and later*), HAWK uses a combination of algorithms to distinguish the basketball hoop targets for the FIRST FRC 2012 Robotics Competition (*Rebound Rumble*). To detect the targets, HAWK uses a series of algorithms to determine and draw a bounding rectangle around the detected targets.

**NOTE:** This project is still in beta mode, so the code is not entirely stable (no known bugs yet) and the target detection is acceptable but not robust.

**Features:** For basic target detection, HAWK requires a bright light to be shown on the retroreflective tape outlining the hoop target. The raw camera image needs to be converted to binary threshold images, set by the user. Then HAWK employs a group of routines to find targets in the image (in order):

1. **FindPoly():** This routine does the inital estimates of potential target positions in the threshold. It returns a vector of OpenCV rectangles it considers to be hoop targets.

2. **TrimToAspectRatio():** The vector of potential targets are then "pruned" to discard false positives based on the approximate aspect ratio of the bounding rectangle.

3. **NestedTarget():** The vector array is then finalized with a test for concentric rectangles with a contour analysis of potential region of interests.
Future releases may include a Haarcascade analysis of the target (within the region of interest) to do additional checks on the target array for false positives.

4. **Trim to Cascade Classifier:** This function is still experimental, but it uses Haar Cascade Classification to confirm the tracked pattern is a target.

There are a series of convenience methods (such as RenderTargets() and CompactRectangle()) that allows the developer to render the targets detected by the tracking functions.
There is also a sorting function that finds the highest target detected on the given image.

**Documentation:** Please view the source code for documentation. 

**License:** BSD.

Copyright (C) 2012, "masoug", all rights reserved.

License Agreement For Open Source Computer Vision Library

 Copyright (C) 2000-2008, Intel Corporation, all rights reserved.

 Copyright (C) 2009-2011, Willow Garage Inc., all rights reserved.

 Third party copyrights are property of their respective owners.

 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:

   * Redistribution's of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.

   * Redistribution's in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.

   * The name of the copyright holders may not be used to endorse or promote
     products derived from this software without specific prior written permission.

 This software is provided by the copyright holders and contributors "as is" and
 any express or implied warranties, including, but not limited to, the implied
 warranties of merchantability and fitness for a particular purpose are disclaimed.
 In no event shall the Intel Corporation, "masoug" or contributors be liable for any direct,
 indirect, incidental, special, exemplary, or consequential damages
 (including, but not limited to, procurement of substitute goods or services;
 loss of use, data, or profits; or business interruption) however caused
 and on any theory of liability, whether in contract, strict liability,
 or tort (including negligence or otherwise) arising in any way out of
 the use of this software, even if advised of the possibility of such damage.
